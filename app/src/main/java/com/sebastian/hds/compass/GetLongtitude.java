package com.sebastian.hds.compass;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Sebastian on 22.05.15.
 */
public class GetLongtitude extends DialogFragment {
    private EditText longtitude;
    private Button cancelBtn, okBtn;

    public GetLongtitude() {

    }

    public interface GetLongtitudeListener {
        void onFinishLongtitude(double longtitude);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_longtitude, container);
        longtitude = (EditText) view.findViewById(R.id.longtitudeField);
        cancelBtn = (Button) view.findViewById(R.id.cancelBtn);
        okBtn = (Button) view.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetLongtitudeListener longtitudeListener = (GetLongtitudeListener) getActivity();
                if (longtitude.getText().toString().isEmpty()) {
                    longtitude.setError("EMPTY FIELD");
                } else {
                    longtitudeListener.onFinishLongtitude(Double.parseDouble(longtitude.getText().toString()));
                    dismiss();
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }
}
