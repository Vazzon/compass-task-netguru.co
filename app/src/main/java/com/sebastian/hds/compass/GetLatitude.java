package com.sebastian.hds.compass;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class GetLatitude extends DialogFragment {
    private EditText latitude;
    private Button cancelBtn, okBtn;


    public GetLatitude() {

    }

    public interface GetLatitudeListener {
        void onFinishLatitude(double latitude);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_latitude, container);
        latitude = (EditText) view.findViewById(R.id.latitudeField);
        cancelBtn = (Button) view.findViewById(R.id.cancelBtn);
        okBtn = (Button) view.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetLatitudeListener latitudeListener = (GetLatitudeListener) getActivity();
                if (latitude.getText().toString().isEmpty()) {
                    latitude.setError("EMPTY FIELD");
                } else {
                    latitudeListener.onFinishLatitude(Double.parseDouble(latitude.getText().toString()));
                    dismiss();
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

}





