package com.sebastian.hds.compass;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends FragmentActivity implements SensorEventListener, GetLatitude.GetLatitudeListener, GetLongtitude.GetLongtitudeListener {

    private SensorManager sm;
    private ImageView rose, navi_arrow;
    Button getLatitude, getLongtitude;
    private Sensor accSensor, magSensor;
    private float degree = 0f, degreex = 0f;
    TextView heading, toDestination;
    boolean isNetworkEnabled=false;
    boolean isGpsEnabled=false;
    boolean mLastAccelerometerSet = false, mLastMagnetometerSet = false;
    float north_direction;
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private float[] values = new float[3];
    private float[] mR = new float[9];
    private float[] mI = new float[9];
    float northDirectionToDegrees;
    float bearingDistance;
    Location locationB = new Location("pointB");
    LocationManager lm;
    Location lastLocation= new Location("LastKnownLocation");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        accSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magSensor = sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        rose = (ImageView) findViewById(R.id.rose);
        getLatitude = (Button) findViewById(R.id.getLatitude);
        getLongtitude = (Button) findViewById(R.id.getLongtitude);
        heading = (TextView) findViewById(R.id.degree);
        toDestination = (TextView) findViewById(R.id.toDestination);
        navi_arrow = (ImageView) findViewById(R.id.navi_arrow);
  lastLocation=lm.getLastKnownLocation(lm.NETWORK_PROVIDER);
    }

        @Override
    public void onFinishLongtitude(double longtitude) {
        locationB.setLongitude(longtitude);
    }

    @Override
    public void onFinishLatitude(double latitude) {
        locationB.setLatitude(latitude);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sm.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_UI);
        sm.registerListener(this, magSensor, SensorManager.SENSOR_DELAY_UI);
    }

    private void showEditDialogLatitude() {
        FragmentManager fm = getSupportFragmentManager();
        GetLatitude getLatitude = new GetLatitude();
        getLatitude.show(fm, "fragmenmt_Latitude");
    }

    private void showEditDialogLongtitude() {
        FragmentManager fm = getSupportFragmentManager();
        GetLongtitude getLongtitude = new GetLongtitude();
        getLongtitude.show(fm, "fragmenmt_Longitutde");
    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this, accSensor);
        sm.unregisterListener(this, magSensor);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if (sensorEvent.sensor == accSensor) {
            System.arraycopy(sensorEvent.values, 0, mLastAccelerometer, 0, sensorEvent.values.length);
            mLastAccelerometerSet = true;
        }
        if (sensorEvent.sensor == magSensor) {
            System.arraycopy(sensorEvent.values, 0, mLastMagnetometer, 0, sensorEvent.values.length);
            mLastMagnetometerSet = true;
        }
        if (mLastAccelerometerSet && mLastAccelerometerSet) {
            SensorManager.getRotationMatrix(mR, mI, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, values);
            north_direction = values[0];

            northDirectionToDegrees = (float) (((Math.toDegrees(north_direction)) + 360) % 360);
            RotateAnimation rotateAnimation = new RotateAnimation(degree, -northDirectionToDegrees, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(210);
            rotateAnimation.setFillAfter(true);
            rose.startAnimation(rotateAnimation);
            degree = -northDirectionToDegrees;


            bearingDistance = ((degree - lastLocation.bearingTo(locationB)) + 360) % 360;
            RotateAnimation rotateAnimation1 = new RotateAnimation(degreex, -bearingDistance, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation1.setDuration(210);
            rotateAnimation1.setFillAfter(true);
            navi_arrow.startAnimation(rotateAnimation1);
            degreex = -bearingDistance;


        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.getLatitude:
                showEditDialogLatitude();
                break;
            case R.id.getLongtitude:
                showEditDialogLongtitude();
                break;
        }
    }


}


